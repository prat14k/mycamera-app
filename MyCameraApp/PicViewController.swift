//
//  PicViewController.swift
//  MyCameraApp
//
//  Created by Prateek Sharma on 27/02/18.
//  Copyright © 2018 Prateek Sharma. All rights reserved.
//

import UIKit

class PicViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    var image : UIImage!
    
    @IBAction func cancelButtonAction(_ sender : UIButton) {
        self.dismiss(animated: true) {
        }
    }
    
    @objc func imageSavedSuccessfully(_ image: UIImage, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer){
        var alertController : UIAlertController!
        
        if error != nil {
            alertController = UIAlertController(title: "Image Not Saved", message: "Image was not saved. Please check whether you have provoded us the permission to save.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (action) in
            }))
        }
        else {
            alertController = UIAlertController(title: "Image Saved Successfully", message: "Click on the Okay button to go back to click", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Okay", style: .default, handler: { (action) in
            self.dismiss(animated: true) {
            }
            }))
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonAction(_ sender : UIButton) {
        UIImageWriteToSavedPhotosAlbum(image, self, #selector(imageSavedSuccessfully(_:didFinishSavingWithError:contextInfo:)), nil)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageView.image = image
    }
}
