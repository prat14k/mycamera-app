//
//  ViewController.swift
//  MyCameraApp
//
//  Created by Prateek Sharma on 27/02/18.
//  Copyright © 2018 Prateek Sharma. All rights reserved.
//

import UIKit
import AVFoundation

enum CameraOutputType {
    case Photo
    case Video
}

enum FlashType {
    case on
    case off
    case auto
}

enum CameraOrientationType {
    case rear
    case front
}


class ViewController: UIViewController {

    var cameraOutputType : CameraOutputType = .Photo
    var cameraFlashType : FlashType = .off
    var cameraOrientationType : CameraOrientationType = .rear
    
    @IBOutlet weak var audioRecordButton: UIButton!
    @IBOutlet weak var cameraCaptureButton: CustomButton!
    @IBOutlet weak var cameraOrientation: UIButton!
    @IBOutlet weak var flashButton: UIButton!
    @IBOutlet weak var recordingType: UIButton!
    @IBOutlet var gestureRecognizer: UILongPressGestureRecognizer!
    
    var isRecording = false
    
    var captureSession = AVCaptureSession()
    var backCameraDevice : AVCaptureDevice?
    var frontCameraDevice : AVCaptureDevice?
    var currentCameraDevice : AVCaptureDevice?
    
    var cameraCaptureDeviceInput : AVCaptureDeviceInput!
    
    var photoOutput : AVCapturePhotoOutput!
    var videoOutput : AVCaptureVideoDataOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        setupInputDevices()
        
        setupCaptureSessionPreset(AVCaptureSession.Preset.photo)
        
        setupCameraDeviceInput()
        
        setupPhotoOutput()
        setupPreviewLayer()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        captureSession.startRunning()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        captureSession.stopRunning()
    }
    
    func setupUI(){
        
        audioRecordButton.setTitle(kicon_audio, for: .normal)
        cameraOrientation.setTitle(kicon_rotate, for: .normal)
        
        var flashtext : String
        switch cameraFlashType {
        case .auto:
            flashtext = kicon_flash_auto
        case .on:
            flashtext = kicon_flash
        case .off:
            flashtext = kicon_flash_off
        }
        flashButton.setTitle(flashtext, for: .normal)
        
        var cameraOutputText : String
        var captureButtonText : String
        switch cameraOutputType {
        case .Photo:
            cameraCaptureButton.removeCircles()
            cameraOutputText = kicon_video
            captureButtonText = kicon_camera_click
            gestureRecognizer.isEnabled = false
        case .Video:
            cameraCaptureButton.addWhiteCircle()
            cameraOutputText = kicon_photo
            gestureRecognizer.isEnabled = true
            captureButtonText = kicon_circle
        }
        recordingType.setTitle(cameraOutputText, for: .normal)
        cameraCaptureButton.setTitle(captureButtonText, for: .normal)
    }
    
    
    @IBAction func recordAudioAction(_ sender: UIButton) {
        
        
        
    }
    
    @IBAction func cameraOrientationChangeAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        captureSession.stopRunning()
        currentCameraDevice = sender.isSelected ? frontCameraDevice : backCameraDevice
        setupCameraDeviceInput()
        
        UIView.transition(with: self.view, duration: 0.5, options: [ .transitionFlipFromRight], animations: nil)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            self?.captureSession.startRunning()
        }
    }
    
    @IBAction func flashTypeChangeAction(_ sender: UIButton) {
    
        
        
    }
    
    
    @IBAction func recordingTypeChangeAction(_ sender: UIButton) {
        
        var cameraOutputText : String
        var captureButtonText : String
        var longPressGestureEnableStatus : Bool
        
        switch cameraOutputType {
        case .Photo:
            cameraOutputType = .Video
            cameraCaptureButton.addWhiteCircle()
            cameraOutputText = kicon_photo
            captureButtonText = kicon_circle
            longPressGestureEnableStatus = true
        case .Video:
            cameraOutputType = .Photo
            cameraCaptureButton.removeCircles()
            cameraOutputText = kicon_video
            captureButtonText = kicon_camera_click
            longPressGestureEnableStatus = false
        }
        recordingType.setTitle(cameraOutputText, for: .normal)
        cameraCaptureButton.setTitle(captureButtonText, for: .normal)
        gestureRecognizer.isEnabled = longPressGestureEnableStatus
        
        cameraCaptureButton.alpha = 0
        UIView.transition(with: self.view, duration: 0.5, options: [ .transitionFlipFromRight ], animations: {
            self.cameraCaptureButton.alpha = 1
        }) { (finished) in
            print(finished)
            
            switch self.cameraOutputType {
            case .Photo:
                self.setupCaptureSessionPreset(AVCaptureSession.Preset.photo)
                self.setupPhotoOutput()
            case .Video:
                self.setupCaptureSessionPreset(AVCaptureSession.Preset.low)
                self.setupVideoOutput()
            }
            
        }
        
    }
    
    @IBAction func longPressGetureAction(_ sender: UILongPressGestureRecognizer) {
        
        switch sender.state {
        case .began:
            print("began")
            self.cameraCaptureButton.addRedCircle()
            UIView.animate(withDuration: 0.6, animations: {
                self.cameraCaptureButton.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
            })
        case .ended:
            print("ended")
            self.cameraCaptureButton.removeRedCircle()
            UIView.animate(withDuration: 0.6, animations: {
                self.cameraCaptureButton.transform = CGAffineTransform.identity
            })
        case .cancelled:
            fallthrough
        case .failed:
            self.cameraCaptureButton.removeRedCircle()
            UIView.animate(withDuration: 0.6, animations: {
                self.cameraCaptureButton.transform = CGAffineTransform.identity
            })
        default:
            print(sender.state.rawValue)
        }
        
    }
    
    @IBAction func recordingButtonAction(_ sender : UIButton) {
        
        if cameraOutputType == .Photo {
            photoOutput.capturePhoto(with: AVCapturePhotoSettings(), delegate: self)
        }
        else{
            
        }
        
    }
    
}


extension ViewController : AVCaptureVideoDataOutputSampleBufferDelegate{
    
    func setupCaptureSessionPreset(_ preset : AVCaptureSession.Preset){
        captureSession.sessionPreset = preset
    }
    
    func setupInputDevices() {
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.unspecified)
        let devices = deviceDiscoverySession.devices
        for device in devices {
            if device.position == AVCaptureDevice.Position.back {
                backCameraDevice = device
            } else if device.position == AVCaptureDevice.Position.front {
                frontCameraDevice = device
            }
        }
        
        currentCameraDevice = backCameraDevice
    }
    
    func setupCameraDeviceInput(){
        if let currentCamera = self.currentCameraDevice {
            do {
                let captureDeviceInput = try AVCaptureDeviceInput(device: currentCamera)
                captureSession.beginConfiguration()
                if cameraCaptureDeviceInput != nil {
                    captureSession.removeInput(cameraCaptureDeviceInput)
                    cameraCaptureDeviceInput = nil
                }
                if captureSession.canAddInput(captureDeviceInput) {
                    cameraCaptureDeviceInput = captureDeviceInput
                    captureSession.addInput(cameraCaptureDeviceInput)
                }
                
                captureSession.commitConfiguration()
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
    
    
    func setupPhotoOutput() {
        
        captureSession.beginConfiguration()
        
        photoOutput = AVCapturePhotoOutput()
                
        photoOutput.setPreparedPhotoSettingsArray([AVCapturePhotoSettings(format: [AVVideoCodecKey : AVVideoCodecJPEG])], completionHandler: nil)
        
        if (captureSession.canAddOutput(photoOutput) == true) {
            if videoOutput != nil {
                captureSession.removeOutput(videoOutput)
            }
            captureSession.addOutput(photoOutput)
        }
        
        captureSession.commitConfiguration()
    }
    
    func setupPreviewLayer() {
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspectFill
        previewLayer.connection?.videoOrientation = .portrait
        previewLayer.frame = self.view.frame
        
        self.view.layer.insertSublayer(previewLayer, at: 0)
    }
    
    
    func setupVideoOutput() {
        
        captureSession.beginConfiguration()
        
        videoOutput = AVCaptureVideoDataOutput()
        videoOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String : NSNumber(value: kCVPixelFormatType_420YpCbCr8BiPlanarFullRange as UInt32)]
        videoOutput.alwaysDiscardsLateVideoFrames = true
        
        if (captureSession.canAddOutput(videoOutput) == true) {
            if photoOutput != nil {
                captureSession.removeOutput(photoOutput)
            }
            captureSession.addOutput(videoOutput)
        }
        
        captureSession.commitConfiguration()
        
        let queue = DispatchQueue(label: "prat14k.MyCamera.VideoOutputQueue")
        videoOutput.setSampleBufferDelegate(self as AVCaptureVideoDataOutputSampleBufferDelegate, queue: queue)
    }
}


extension ViewController : AVCapturePhotoCaptureDelegate {
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?, previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?, resolvedSettings: AVCaptureResolvedPhotoSettings, bracketSettings: AVCaptureBracketedStillImageSettings?, error: Error?) {
        
        if let imageData = AVCapturePhotoOutput.jpegPhotoDataRepresentation(forJPEGSampleBuffer: photoSampleBuffer!, previewPhotoSampleBuffer: previewPhotoSampleBuffer) {
            if let image = UIImage(data: imageData) {
                
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                
                if let vc = storyBoard.instantiateViewController(withIdentifier: "PicViewVC") as? PicViewController {
                    vc.image = image
                    self.present(vc, animated: true, completion: {
                        print("Modal done")
                    })
                }
            }
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
    }
    
    func captureOutput(_ output: AVCaptureOutput, didDrop sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
    }
    
}

