//
//  CustomButton.swift
//  MyCameraApp
//
//  Created by Prateek Sharma on 28/03/18.
//  Copyright © 2018 Prateek Sharma. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    lazy var whiteCircle : CAShapeLayer = {
        let pathLayer = CAShapeLayer()
        pathLayer.strokeColor = UIColor.white.cgColor
        pathLayer.fillColor = nil
        pathLayer.lineWidth = 7
        pathLayer.lineJoin = kCALineJoinRound
        pathLayer.frame = self.bounds
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height), cornerRadius: self.frame.size.width / 2)
        pathLayer.path = path.cgPath
        
        return pathLayer
    }()
    
    lazy var redCircle : CAShapeLayer = {
        let pathLayer = CAShapeLayer()
        pathLayer.strokeColor = UIColor.red.cgColor
        pathLayer.fillColor = nil
        pathLayer.lineWidth = 7
        pathLayer.lineJoin = kCALineJoinRound
        pathLayer.frame = self.bounds
        let path = UIBezierPath(roundedRect: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height), cornerRadius: self.frame.size.width / 2)
        pathLayer.path = path.cgPath
        
        return pathLayer
    }()

    func addWhiteCircle() {
        self.layer.insertSublayer(whiteCircle, at: 0)
    }
    func addRedCircle() {
        
        redCircle.isHidden = false
        
        self.layer.insertSublayer(redCircle, at: 1)
        
        let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
        pathAnimation.duration = 4.0
        pathAnimation.fromValue = 0.0
        pathAnimation.toValue = 1.0
        redCircle.add(pathAnimation, forKey: "strokeEnd")
    }
    
    func removeRedCircle() {
        
        redCircle.isHidden = true
        redCircle.removeAllAnimations()
        redCircle.removeFromSuperlayer()
        
    }
    
    func removeCircles() {
        redCircle.removeFromSuperlayer()
        whiteCircle.removeFromSuperlayer()
    }
    
//        self.layer.insertSublayer(pathLayer, at: 0)
    
//    let pathAnimation = CABasicAnimation(keyPath: "strokeEnd")
//    pathAnimation.duration = 3.0
//    pathAnimation.fromValue = 0.0
//    pathAnimation.toValue = 1.0
//    pathLayer.add(pathAnimation, forKey: "strokeEnd")
    
    
}
