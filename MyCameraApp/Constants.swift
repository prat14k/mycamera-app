//
//  Constants.swift
//  MyCameraApp
//
//  Created by Prateek Sharma on 27/03/18.
//  Copyright © 2018 Prateek Sharma. All rights reserved.
//



let kicon_menu = "\u{e800}"
let kicon_photo = "\u{e801}"
let kicon_flash = "\u{e802}"
let kicon_camera_click = "\u{e803}"
let kicon_video = "\u{e804}"
let kicon_flash_auto = "\u{e805}"
let kicon_rotate = "\u{e806}"
let kicon_flash_off = "\u{e807}"
let kicon_audio = "\u{e808}"
let kicon_cancel = "\u{e809}"
let kicon_circle = "\u{f111}"
let kicon_recording = "\u{f192}"
